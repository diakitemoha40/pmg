import React from 'react';
import SignIn from 'components/SignInAndSignUp/SignIn/SignIn';
import SignUp from 'components/SignInAndSignUp/SignUp/SignUp';
import { Switch, Route } from 'react-router-dom';

/* import SignInAndSignUp from 'components/SignInAndSignUp/SignInAndSignUp';
 */
function App() {
  return (
    <div>
      {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
      <Switch>
        <Route path="/" component={SignIn} exact />
        <Route path="/inscription" component={SignUp} />
      </Switch>
    </div>
  );
}

export default App;
