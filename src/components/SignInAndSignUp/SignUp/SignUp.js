import React from 'react';
import Logo from './logo.png';
import SlideText from '../SlideText';
import TabsSignUp from './TabsSignUp';

import './styles.css';

export default function SignUp() {
  return (
    <div className="mx-auto flex flex-wrap flex-col md:flex-row items-center ">
      {/*         Left Col
       */}

      <div className="flex flex-col w-full md:w-3/5 pl-3  md:text-left h-screen">
        <button
          type="submit"
          className=" top-0 w-45 h-16 p-2 self-end btn-home mt-10 mr-4 text-white font-bold rounded-md shadow-lg focus:outline-none focus:shadow-outline transform transition hover:scale-105 duration-300 ease-in-out"
        >
          Retour à l &#39; acceuil
        </button>
        <div className="mx-auto justify-center items-center w-full pl-16 h-screen mt-12">
          <img src={Logo} alt="logo" className="pb-4" />
          <h1 className=" text-4xl mb-8 leading-tight max-w-5xl">
            <span className=" text-4xl font-bold leading-tight mr-2">
              Inscrivez-vous
            </span>
            pour accéder à PME Gestion Facile
          </h1>
          <p className="leading-normal mb-4 text-xl max-w-5xl">
            In his tractibus navigerum nusquam visitur flumen sed in locis
            plurimis aquae suapte natura calentes emergunt ad usus aptae
          </p>
          <div className="flex flex-col items-center justify-center">
            <p>Vous êtes :</p>
            <TabsSignUp />
            {/*             <p>
              <TabsSignUp />
            </p> */}
          </div>
        </div>

        <p className="text-center ">2020 Akilcab • Tout droit réservé</p>
      </div>

      {/*         Right Col
       */}
      <div className="w-full relative md:w-2/5 py-6 text-center general-color h-screen bgImageGrille">
        <div className="absolute inset-x-0 bottom-0 ">
          <SlideText />
        </div>
      </div>
    </div>
  );
}
