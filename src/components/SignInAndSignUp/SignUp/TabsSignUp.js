import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import './styles.css';
import TabManager from './TabManager';
import UserIcon from './Iconfeather-user.png';
import Meuble from './meuble.png';

const TABS = [
  {
    label: 'Personne moral',
    value: 1,
    icon: <img src={UserIcon} alt="logo" />,
  },

  {
    label: 'Personne physique',
    value: 2,
    icon: <img src={Meuble} alt="logo" />,
  },
];
export default function TabsSignUp() {
  const [activeTab, handleTab] = useState(1);
  return (
    <div className="App">
      <TabManager tabs={TABS} activeTab={activeTab} handleTab={handleTab} />
      <div className="mt-7">
        <div>
          {activeTab === 1 ? (
            <div className="grid grid-cols-2">
              <div>
                <input
                  type="text"
                  name="raisonSociale"
                  className="appearance-none  block w-11/12 h-16 mb-8 px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                  placeholder="Raison sociale"
                />
                <input
                  type="text"
                  name="Email"
                  className="appearance-none relative block w-11/12 h-16 mb-8 px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                  placeholder="Email"
                />
                <input
                  type="text"
                  name="mdp"
                  className="appearance-none relative block w-11/12 h-16 mb-8 px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                  placeholder="Mot de passe"
                />
                <p className="text-center float-left pt-2">
                  Déjà un compte ?
                  <a
                    href="boxing.html"
                    className="font-medium text-blue-600 hover:text-blue-500 ml-1"
                  >
                    <Link to="/"> Connectez-Vous </Link>
                  </a>
                </p>
              </div>
              <div>
                <input
                  type="text"
                  name="cmdp"
                  className="appearance-none relative block w-11/12 h-16 mb-8 px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                  placeholder="Comfirmer mot de passe"
                />
                <input
                  type="text"
                  name="contact"
                  className="appearance-none relative block w-11/12 h-16 mb-8 px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                  placeholder="contact"
                />
                <input
                  type="text"
                  name="Contact2"
                  className="appearance-none relative block w-11/12 h-16 mb-8 px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                  placeholder="Contact 2"
                />
                <button
                  type="submit"
                  className="mx-auto lg:mx-0 w-8/12 pl-10 ml-2 h-16 general-color text-white font-bold rounded-md shadow-lg focus:outline-none focus:shadow-outline transform transition hover:scale-105 duration-300 ease-in-out"
                >
                  Connexion
                </button>
              </div>
            </div>
          ) : (
            <div className="grid grid-cols-2">
              <div>
                <input
                  type="text"
                  name="noms"
                  className="appearance-none  block w-11/12 h-16 mb-8 px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                  placeholder="Nom"
                />
                <input
                  type="text"
                  name="prenoms"
                  className="appearance-none relative block w-11/12 h-16 mb-8 px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                  placeholder="Prenoms"
                />
                <input
                  type="text"
                  name="Email"
                  className="appearance-none relative block w-11/12 h-16 mb-8 px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                  placeholder="Email"
                />
                <p className="text-center float-left pt-2">
                  Déjà un compte ?
                  <a
                    href="boxing.html"
                    className="font-medium text-blue-600 hover:text-blue-500 ml-1"
                  >
                    <Link to="/"> Connectez-Vous </Link>
                  </a>
                </p>
              </div>
              <div>
                <input
                  type="text"
                  name="mdp"
                  className="appearance-none relative block w-11/12 h-16 mb-8 px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                  placeholder="Mot de passe"
                />
                <input
                  type="text"
                  name="cmdp"
                  className="appearance-none relative block w-11/12 h-16 mb-8 px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                  placeholder="Confirmer mot de passe"
                />
                <input
                  type="text"
                  name="contact"
                  className="appearance-none relative block w-11/12 h-16 mb-8 px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                  placeholder="Contact"
                />
                <button
                  type="submit"
                  className="mx-auto lg:mx-0 w-8/12 pl-10 ml-2 h-16 general-color text-white font-bold rounded-md shadow-lg focus:outline-none focus:shadow-outline transform transition hover:scale-105 duration-300 ease-in-out"
                >
                  Connexion
                </button>
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
