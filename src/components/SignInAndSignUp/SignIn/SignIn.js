import React from 'react';
import { Link } from 'react-router-dom';
import SlideText from '../SlideText';
import Logo from './logo.png';
import '../SignUp/styles.css';

export default function SignIn() {
  return (
    <div className="mx-auto flex flex-wrap flex-col md:flex-row items-center ">
      {/*         Left Col
       */}

      <div className="flex flex-col w-full md:w-2/5  md:text-left h-screen">
        <button
          type="submit"
          className=" top-0 w-45 h-16 p-2 self-end btn-home mt-10 mr-4 text-white font-bold rounded-md shadow-lg focus:outline-none focus:shadow-outline transform transition hover:scale-105 duration-300 ease-in-out"
        >
          Retour à l &#39; acceuil
        </button>
        <div className="mx-auto justify-center items-center w-full h-screen ml-24 mt-12">
          <img src={Logo} alt="logo" className="pb-8" />
          <h1 className=" text-5xl mb-8 leading-tight max-w-2xl">
            <span className=" text-5xl font-bold leading-tight mr-2">
              Connectez vous
            </span>
            pour accedez à PME Gestion Facile
          </h1>
          <p className="leading-normal mb-8 text-xl max-w-2xl">
            In his tractibus navigerum nusquam visitur flumen sed in locis
            plurimis aquae suapte natura calentes emergunt ad usus aptae
          </p>
          <form className="w-full mx-auto">
            <input
              type="text"
              name="Email"
              className="appearance-none relative block w-9/12 h-16 mb-8 px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
              placeholder="Email"
            />
            <input
              type="text"
              name="Email"
              className="appearance-none relative block w-9/12 h-16 px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
              placeholder="Mot de passe"
            />

            <div className="flex items-center justify-between mt-3">
              <div className="flex items-center">
                <label
                  htmlFor="remember_me"
                  className="ml-2 block text-sm text-gray-900"
                >
                  <input
                    id="remember_me"
                    name="remember_me"
                    type="checkbox"
                    className="h-5 w-5 general-color focus:general-color border-gray-300 rounded mr-1"
                  />
                  Se souvenir de moi
                </label>
              </div>

              <div className="text-sm pr-36">
                <a
                  href="boxing.html"
                  className="font-medium mr-14 text-blue-600 hover:text-blue-500"
                >
                  Mot de passe oublié ?
                </a>
              </div>
            </div>
            <button
              type="submit"
              className="mx-auto lg:mx-0 w-9/12 h-16 general-color text-white font-bold rounded-md my-6 py-4 px-8 shadow-lg focus:outline-none focus:shadow-outline transform transition hover:scale-105 duration-300 ease-in-out"
            >
              Connexion
            </button>
            <p className="text-center pr-56">
              Nouveau sur AkilCab ?
              <a
                href="boxing.html"
                className="font-medium text-blue-600 hover:text-blue-500 ml-1"
              >
                <Link to="inscription">Inscrivez-Vous</Link>
              </a>
            </p>
          </form>
        </div>

        <p className="text-center ">2020 Akilcab • Tout droit réservé</p>
      </div>

      {/*         Right Col
       */}
      <div className="w-full relative md:w-3/5 py-6 text-center general-color h-screen bgImageGrille">
        <div className="absolute inset-x-0 bottom-0 ">
          <SlideText />
        </div>
      </div>
    </div>
  );
}
