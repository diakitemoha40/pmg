// eslint-disable jsx-props-no-spreading
import React from 'react';
import './styleSlider.css';

export default function SlideText() {
  return (
    <div className="s-wrap">
      <div className="s-move">
        <div className="slide">
          <h2 className="text-2xl mb-8">
            Accédez à vos prévisions d'activité et votre réalisation en un coup
            d'oeil
          </h2>
          <p>
            The episode orbits? The panic overwhelms a fuse. The major lurks
            below the shower!
          </p>
        </div>
        <div className="slide">
          <h2 className="text-2xl mb-8">
            Gérez vos clients, vos devis, propositions et factures et veillez à
            leur parfaite satisfaction
          </h2>
          <p>
            The lasting astronomer balances the counter reminder. The trap hires
            the paradox.
          </p>
        </div>
        <div className="slide">
          <h2 className="text-2xl mb-8">Gérez vos Fournisseurs</h2>
          <p>
            Her birthday calculates past a juice! The envy succeeds across an
            evident jelly. An afternoon shifts opposite a bust.
          </p>
        </div>
        <div className="slide">
          <h2 className="text-2xl mb-8">
            Demande de devis, commande, livraison et factures
          </h2>
          <p>
            A distributed actor pilots the null pencil. The wild wolfs a damp
            cage inside the breach.
          </p>
        </div>
      </div>
    </div>
  );
}
